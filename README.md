# ICFEM2023-POASTD

## Description

This project is related to the paper "Proving Local Invariants in ASTDs" submitted in ICFEM 2023.

## Rodin-proof-case-study.zip

This .zip file that can be imported in RODIN. It contains two contexts that are contexts of the case study in the paper "Proving Local Invariants in ASTDs" published in ICFEM 2023. The context "case-study-imprecise" is related to the original case study while "case-study" is related to the case study after the declared modification in Section 3.5.

## preview_rodin_case-study-imprecise.pdf

This .pdf file is a preview of the context "case-study-imprecise" used in Rodin.

## preview_rodin_case-study.pdf

This .pdf file is a preview of the context "case-study" used in Rodin.

## spec.eastd

This .eastd file corresponds to the specification of the modified case study. In order to view the specification of the ASTD, please use eASTD, the editor of ASTD specifications. Instructions to download, install and use the editor can be found [here](https://github.com/DiegoOliveiraUDES/ASTD-tools).

## spec.json

This .json file contains the ASTD specification of the modified case study in the format required by cASTD, the ASTD compiler. In order to run the ASTD, please use cASTD, the compiler of ASTD specifications. Instructions to download, install and use the compiler can be found [here](https://github.com/DiegoOliveiraUDES/ASTD-tools).